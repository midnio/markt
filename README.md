# Usage
```python
from markt import Markt
t = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porttitor tempor porta."
print(Markt(t).mark(['dolor', 'sit', 'consectetur']))
```
And the output is
```
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porttitor tempor porta.
            ----- ---       -----------
```
And you can change marking style too!
```python
from markt import Markt
t = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porttitor tempor porta."
print(Markt(t).mark(['dolor', 'sit', 'consectetur'], '!'))
```
Output:
```
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porttitor tempor porta.
            !!!!! !!!       !!!!!!!!!!!
```
