class Markt:
  def __init__(self, t):
    self.t = t
  def finde(self, w):
    temp = ''
    i = 0
    for a in self.t:
      temp += a
      if w in temp:
        return i
      i += 1
  def finds(self, w):
    temp = ''
    i = 0
    for a in self.t:
      temp += a
      if w in temp:
        return i - len(w) + 1
      i += 1
  def mark(self, ws, ms='-'):
    a = self.t
    marked = self.t + '\n'
    for w in ws:
      if w not in self.t:
        continue
      marked += self.finds(w) * ' ' + ms * len(w)
      self.t = self.t[self.finde(w) + 1:]
    self.t = a
    return marked
